# MyGifs API Documentation

- [MyGifs API Documentation](#MyGifs-API-Documentation)
  - [General](#General)
- [Auth](#Auth)
  - [Generate a login token](#Generate-a-login-token)
    - [Request](#Request)
      - [Headers](#Headers)
    - [Form Fields](#Form-Fields)
    - [Response](#Response)
      - [Headers](#Headers-1)
      - [Content](#Content)
- [Users](#Users)
  - [Register a new user](#Register-a-new-user)
    - [Request](#Request-1)
      - [Headers](#Headers-2)
    - [JSON Fields](#JSON-Fields)
    - [Response](#Response-1)
      - [Headers](#Headers-3)
      - [Content](#Content-1)
  - [Get user details](#Get-user-details)
    - [Request](#Request-2)
      - [Headers](#Headers-4)
    - [Response](#Response-2)
      - [Headers](#Headers-5)
      - [Content](#Content-2)
  - [Delete a user](#Delete-a-user)
    - [Request](#Request-3)
  - [Update user details](#Update-user-details)
    - [Request](#Request-4)
  - [Gifs](#Gifs)
  - [Upload a gif](#Upload-a-gif)
    - [Request](#Request-5)
      - [Headers](#Headers-6)
      - [Form Fields](#Form-Fields-1)
    - [Response](#Response-3)
      - [Headers](#Headers-7)
      - [Content](#Content-3)
  - [Get gif details](#Get-gif-details)
    - [Request](#Request-6)

## General


---
# Auth

## Generate a login token

### Request 

    POST /auth

#### Headers  
`content-type: application/x-www-form-urlencoded`

### Form Fields  
| Field         | Type      | Details  | 
| ------------- | --------- | -------- |
| `userName`    | *string*  | required |
| `password`    | *string*  | required |


### Response

`200` - Successful login.  
Returns a login token:

#### Headers  

`content-type: application/json`

#### Content
```json
    {
        "token": "5d140f3babc6468d094bcc86"
    }
```

---
# Users


## Register a new user

### Request 

    POST /users

#### Headers  
`content-type: application/json`

### JSON Fields  
| Field             | Type      | Details  | 
| ------------------| --------- | -------- |
| `userName`        | *string*    | required |
| `password`        | *string*    | required |
| `fullName`        | *string*    | required |
| `emailAddress`    | *string*    | required. validates email formatted string |


### Response

`200` - User successfully registered.  

*Returns a login token*

#### Headers  

`content-type: application/json`

#### Content
```json
    {
        "dateRegistered": "2019-06-26T23:39:14.705317+00:00",
        "emailAddress": "buddy@heb.com",
        "fullName": "HEB Buddy"
    }
```


## Get user details

> NOTE: Not implemented

### Request

    GET /users/<string:name>

#### Headers  
`content-type: application/json`  
`auth-token: xxxxxxxxxxxxxxxxxx` 

> Requires a valid authentication via the `auth-token` header


### Response

`200` - Successfully returns requested user details.  

#### Headers  

`content-type: application/json`

#### Content
```json
    {
        "dateRegistered": "2019-06-26T23:39:14.705317+00:00",
        "emailAddress": "buddy@heb.com",
        "fullName": "HEB Buddy"
    }
```


## Delete a user

> NOTE: Not implemented

### Request

    DELETE /users/<string:name>


## Update user details

> NOTE: Not implemented

### Request

    PATCH /users/<string:name>

---
## Gifs 

A `gif` can be uplaoded and categorized in this user profile. 
The gif is uploaded as the user to the Giphy API.


## Upload a gif

### Request

    POST /gifs

> Uplaods to Giphy API are currently not supported due to instability of their Upload API.

#### Headers 

`conent-type: application/x-www-form-urlencoded`  
`auth-token: xxxxxxxxxxxx`

#### Form Fields
| Field     | Type      | Details   |
| --------- | --------- | --------- |
| `tags`    | *string*  | comma delimited list of tags |
| `content` | *string*  | binary file content. ***(not allowed with `sourceUrl`)*** |
| `sourceUr`| *string*  | web url for a gif to configure in Giphy. ***(not allowed with `content`)***|

### Response

`200` - Successfully uploaded the gif


#### Headers  

`content-type: application/json`

#### Content
```json
    {
        "giphyID": "3mgx7HCly8MKD9T6HV",
        "userName": "buddy"
        "uploadDate": "2019-06-26T23:39:14.705317+00:00",
        "tags": "happy,excited",
        "sourceUrl": "https://giphy.com/gifs/mls-happy-excited-3mgx7HCly8MKD9T6HV"
    }
```

## Get gif details

### Request
Return the list of gifs that were uploaded by the user

    GET /gifs

`auth-token: xxxxxxxxxxxx`



