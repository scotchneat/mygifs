from datetime import datetime

import mock
import pytest
from marshmallow import fields
from marshmallow.schema import Schema

from mygifs.data import custom_fields
from mygifs.data.db import Connection, InsertFailed

COLLECTION = 'test'


@pytest.fixture(scope='function')
def mock_connection():
    with mock.patch('mygifs.data.db.MongoClient')as m_db:
        c = Connection()
        c._db[COLLECTION] = mock.Mock()
        return c


@mock.patch('mygifs.data.db.MongoClient')
def test_db_exists(mock_db):
    data = Connection()
    data._db[COLLECTION].count_documents.return_value = 0
    exists = data.exists('collection', {})

    assert not exists


@mock.patch('mygifs.data.db.MongoClient')
def test_db_save_and_get(mock_db):
    expected = 'fake id'

    insert_results_mock = mock.Mock()
    insert_results_mock.acknowledged = True
    insert_results_mock.inserted_id = expected

    data = Connection()
    data._db[COLLECTION].insert_one.return_value = insert_results_mock
    actual = data.save('collection', {})

    assert actual == expected


class Fake:
    def __init__(self, _id, str_val, dt_val):
        self._id = _id
        self.str_val = str_val
        self.dt_val = dt_val


class FakeSchema(Schema):
    _id = fields.String()
    str_val = fields.String(data_key='strVal')
    dt_val = custom_fields.DateTimeFormatted(data_key='dtVal')


def fake_item_to_dict(i):
    return {'_id': i[0], 'strVal': i[1], 'dtVal': i[2]}


@mock.patch('mygifs.data.db.MongoClient')
def test_get_all(m_db):
    test_data = [
        ('id1', 'string one', datetime.utcnow())
    ]

    schema = FakeSchema()
    fake_list = [Fake(*i) for i in test_data]
    expected = schema.dump(fake_list, many=True)

    m_db_resp = [fake_item_to_dict(i) for i in test_data]
    data = Connection()
    data._db[COLLECTION].find.return_value = m_db_resp

    resp = Connection().get_all('fake', {}, FakeSchema, Fake)

    actual = schema.dump(resp, many=True)

    assert actual == expected


@mock.patch('mygifs.data.db.MongoClient')
def test_db_insert_failed(mock_db):
    insert_results_mock = mock.Mock()
    insert_results_mock.acknowledged = False

    data = Connection()
    data._db[COLLECTION].insert_one.return_value = insert_results_mock

    with pytest.raises(InsertFailed):
        data.save('collection', {})
