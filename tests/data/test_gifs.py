from datetime import datetime

import pytest
from mock import patch

from mygifs.data.gifs import GifData, NewGif, Gif


@pytest.fixture(scope='module')
def gif_details():
    user = 'username'
    giphy_id = 'one'
    upload_date = datetime.utcnow()
    tags = 'tag1,tag2',
    source_url = 'https://fake.url/for/image.gif'
    return user, giphy_id, upload_date, tags, source_url


@pytest.fixture(scope='module')
def db_response(gif_details):
    user, giphy_id, upload_date, tags, source_url = gif_details
    return Gif(
        _id='someid',
        user_name=user,
        giphy_id=giphy_id,
        upload_date=upload_date,
        tags=tags,
        source_url=source_url
    )


@pytest.fixture(scope='function')
def db_mock():
    with patch('mygifs.data.db.Connection') as m_db:
        yield m_db


def test_insert(db_mock, gif_details, db_response):
    user, giphy_id, upload_date, tags, source_url = gif_details
    db_mock.get_by_id.return_value = db_response

    new_gif = NewGif(user_name=user, tags=tags, source_url=source_url, giphy_id=giphy_id)
    actual = GifData(db_mock).insert(new_gif)

    assert (actual.user_name, actual.giphy_id, actual.upload_date, actual.tags, actual.source_url) == gif_details


def test_get(db_mock, gif_details, db_response):
    user, giphy_id, upload_date, tags, source_url = gif_details
    db_mock.get.return_value = db_response

    actual = GifData(db_mock).get(user_name=user, giphy_id=giphy_id)

    assert (actual.user_name, actual.giphy_id, actual.upload_date, actual.tags, actual.source_url) == gif_details

