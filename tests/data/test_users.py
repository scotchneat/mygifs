import pytest
from mock import patch

from mygifs.data.users import UserExistsException, UserData, NewUser


@patch('mygifs.data.db.Connection')
def test_register_success(db_mock):
    fake_id = 'some id'
    full_name = 'test guy'
    password = 'testy'
    email_address = 'test@test.com'
    fake_doc = {'_id': fake_id, 'fullName': full_name,
                'password': password, 'emailAddress': email_address}
    db_mock.save.return_value = fake_id
    db_mock.exists.return_value = False
    db_mock.get.return_value = fake_doc

    u = NewUser(name=fake_id, full_name=full_name, password=password, email_address=email_address)

    actual = UserData(db_mock).register(u)
    assert actual == fake_id


@patch('mygifs.data.db.Connection')
def test_register_exists(db_mock):
    db_mock.save.return_value = 'some id'
    db_mock.exists.return_value = True

    u = NewUser(
        name='test',
        full_name='test guy',
        password='testy',
        email_address='test@test.com'
    )

    with pytest.raises(UserExistsException):
        UserData(db_mock).register(u)
