from setuptools import setup, find_packages

setup(
    name='mygifs',
    description='An API which allows users to manage their giphy.com gifs',
    version='0.0.1',
    url='https://gitlab.com/scotchneat/mygifs',
    license='',
    author='scotchneat',
    author_email='scotch.neat@live.com',
    install_requires=[
        'flask==1.0.2',
        'docopt==0.6.2',
        'pymongo==3.8.0',
        'requests==2.22.0',
        'giphy-client==1.0.0',
        'marshmallow==3.0.0rc7',
        'stringcase==1.2.0'
    ],
    extras_require={
        'test': ['flake8', 'mock', 'pytest', 'pytest-cov']
    },
    package_dir={
        'mygifs': 'mygifs'
    },
    packages=find_packages(exclude=['tests'])
)
