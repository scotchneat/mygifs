import json
from typing import Type, TypeVar, List

from bson import ObjectId
from marshmallow.schema import Schema
from pymongo import MongoClient, errors as mongo_errors
from pymongo.cursor import Cursor
from pymongo.database import Database
from pymongo.errors import DuplicateKeyError

from mygifs.data import ConnectionFailure, InsertFailed, InvalidKey
from mygifs.utils import json_serializer_default

DEFAULT_MONGO_URL = 'mongodb://localhost/mygifs'
T = TypeVar('T')


def _process_data(data: dict, schema: Type[Schema], output: Type[T]) -> Type[T]:
    json_data = json.dumps(data, default=json_serializer_default)
    data = schema().loads(json_data)
    return output(**data)


def _process_cursor(data: dict, schema: Type[Schema], output: Type[T]) -> List[Type[T]]:
    return [_process_data(i, schema=schema, output=output) for i in data]


class Connection:

    def __init__(self, mongo_url: str = DEFAULT_MONGO_URL):
        """ Initializes the database client
        :param mongo_url: a mongodb url [default: {DEFAULT_MONGO_URL}]
        """.format(**globals())
        self._mongo_url = mongo_url

    @property
    def _db(self) -> Database:
        try:
            client = MongoClient(self._mongo_url)
            return client.get_default_database()
        except mongo_errors.ConnectionFailure as e:
            raise ConnectionFailure(f'Error connecting to the database: {e}')

    def _setup_indexes(self):
        # TODO: configure indexes on collections
        pass

    def exists(self, collection: str, filter_: dict) -> bool:
        return self._db[collection].count_documents(filter_) > 0

    def save(self, collection: str, doc: dict) -> str:
        results = self._db[collection].insert_one(doc)
        if results.acknowledged:
            return str(results.inserted_id)
        else:
            raise InsertFailed('Document not inserted')

    def save_and_get(self, collection: str, data: dict, schema: Type[Schema], t: Type[T]) -> Type[T]:
        try:
            results = self._db[collection].insert_one(data)
        except DuplicateKeyError as ex:
            raise InvalidKey(str(ex))

        if results.acknowledged:
            gif_data = self._db[collection].find_one(ObjectId(results.inserted_id))
            return _process_data(gif_data, schema=schema, output=t)
        else:
            raise InsertFailed('Document not inserted')

    # def get(self, collection: str, query: dict) -> dict:
    #     return self._db[collection].find_one(query)

    def get(self, collection: str, query: dict, schema: Type[Schema], output: Type[T]) -> Type[T]:
        data = self._db[collection].find_one(query)
        return _process_data(data, schema, output)

    def get_by_id(self, collection: str, _id: str, schema: Type[Schema], output: Type[T]) -> Type[T]:
        data = self._db[collection].find_one(ObjectId(_id))
        return _process_data(data, schema=schema, output=output)

    def get_all(self, collection: str, query: dict, schema: Type[Schema], output: Type[T], *,
                skip: int = 0, limit: int = 0) -> List[Type[T]]:
        cursor = self._db[collection].find(query)
        if skip > 0 and limit > 0:
            cursor = cursor.skip(skip)
        if limit > 0:
            cursor = cursor.limit()
        return [_process_data(i, schema=schema, output=output) for i in cursor]

    def get_all_new(self, collection: str, query: dict, skip: int = 0, limit: int = 5) -> Cursor:
        cursor = self._db[collection].find(query)
        if skip > 0 and limit > 0:
            cursor = cursor.skip(skip)
        if limit > 0:
            cursor = cursor.limit()
        return cursor
