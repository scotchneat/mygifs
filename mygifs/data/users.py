from datetime import datetime
from hashlib import sha256

from marshmallow import Schema, fields

from mygifs.data import InvalidKey
from mygifs.data.db import Connection, InsertFailed


class UserError(Exception):
    pass


class UserExistsException(UserError):
    pass


def encrypt_text(plain_text):
    return sha256(plain_text.encode()).hexdigest()


class User:
    def __init__(self, name: str = None, full_name: str = None, password: str = None, email_address: str = None,
                 date_registered: datetime = datetime.utcnow(), verified=False):
        self.name = name
        self.full_name = full_name
        self.password = password
        self.email_address = email_address
        self.date_registered = date_registered
        self.verified = verified

    def verify_password(self, plain_text):
        return self.password == encrypt_text(plain_text)

    def to_data_doc(self):
        doc = {}
        if hasattr(self, 'name'):
            doc['_id'] = self.name
        if hasattr(self, 'full_nameame'):
            doc['fullName'] = self.full_name
        if hasattr(self, 'password'):
            doc['password'] = self.password
        if hasattr(self, 'email_address'):
            doc['emailAddress'] = self.email_address
        if hasattr(self, 'date_registered'):
            doc['dateRegistered'] = self.date_registered
        if hasattr(self, 'verified'):
            doc['verified'] = self.verified

        return doc

    @classmethod
    def from_data_doc(cls, doc: dict):
        user_name = doc.get('_id')
        full_name = doc.get('fullName')
        password = doc.get('password')
        email_address = doc.get('emailAddress')
        date_registered = doc.get('dateRegistered')
        verified = doc.get('verified')
        return User(name=user_name, full_name=full_name, password=password, email_address=email_address,
                    date_registered=date_registered, verified=verified)


class NewUser(User):
    def __init__(self, name: str = None, full_name: str = None, password: str = None, email_address: str = None):
        super().__init__(name, full_name, encrypt_text(password), email_address, datetime.utcnow(), False)


class UserSchema(Schema):
    name = fields.String(data_key='_id')
    full_name = fields.String(data_key='fullName')
    password = fields.String()
    email_address = fields.String(data_key='emailAddress')
    date_registered = fields.DateTime(data_key='dateRegistered')
    verified = fields.Bool()


class UserData:
    COLLECTION_NAME = 'users'

    def __init__(self, db_: Connection):
        self._db = db_

    def register(self, user: NewUser) -> str:
        """Registers a new user

        :returns str: The document ID of the saved user.
        :raises InsertFailed
        """
        email_field = getattr(UserSchema().declared_fields['email_address'], 'data_key')
        if self._db.exists(self.COLLECTION_NAME, {email_field: user.email_address}):
            raise UserExistsException()

        try:
            inserted_id = self._db.save(self.COLLECTION_NAME, user.to_data_doc())
            return inserted_id
        except InvalidKey:
            raise UserExistsException()
        except InsertFailed as ex:
            raise UserError('Failed to insert user', ex)

    def get(self, user_name) -> User:
        """ Fetches user details from the database.
        :param user_name: the user_name for the requested user details
        :return: the requested User instance
        """
        q = {'_id': user_name}
        user = self._db.get(self.COLLECTION_NAME, q, UserSchema, User)
        return user
