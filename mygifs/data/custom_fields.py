from marshmallow.fields import DateTime

from mygifs.constants import DT_FORMAT


class DateTimeFormatted(DateTime):
    class Meta:
        datetimeformat = DT_FORMAT
