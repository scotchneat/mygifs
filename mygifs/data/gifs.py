from datetime import datetime

from marshmallow import Schema, fields, post_dump

from mygifs.constants import GIF_CONTENT_AND_SOURCE_URL_MESSAGE
from mygifs.data import InsertFailed, custom_fields
from mygifs.data.db import Connection


class GifDataError(Exception):
    pass


class Gif:
    def __init__(self, _id: str = None, user_name: str = None, giphy_id: str = None, upload_date: datetime = None,
                 tags: str = None, source_url: str = None):
        """Represents the uploaded gif
        :param giphy_id: unique id generated from Giphy upload
        :param user_name: the user which uploaded the gif
        :param upload_date: date file is uploaded
        :param tags: comma delimited string of descriptive tags
        """
        if _id:
            self._id = _id
        if giphy_id:
            self.giphy_id = giphy_id
        if user_name:
            self.user_name = user_name
        if upload_date:
            self.upload_date = upload_date
        if tags:
            self.tags = tags
        if source_url:
            self.source_url = source_url

    def to_data_doc(self):
        doc = {}
        if hasattr(self, '_id'):
            doc['_id'] = self._id
        if hasattr(self, 'giphy_id'):
            doc['giphyId'] = self.giphy_id
        if hasattr(self, 'user_name'):
            doc['userName'] = self.user_name
        if hasattr(self, 'upload_date'):
            doc['uploadDate'] = self.upload_date
        if hasattr(self, 'tags'):
            doc['tags'] = self.tags
        if hasattr(self, 'source_url'):
            doc['sourceUrl'] = self.source_url
        return doc

    @classmethod
    def from_data_doc(cls, doc: dict):
        _id = doc.get('_id')
        giphy_id = doc.get('giphyId')
        user_name = doc.get('userName')
        upload_date = doc.get('uploadDate')
        tags = doc.get('tags')
        source_url = doc.get('sourceUrl')
        g = Gif(_id=_id, user_name=user_name, giphy_id=giphy_id, upload_date=upload_date, tags=tags,
                source_url=source_url)


class NewGif(Gif):
    def __init__(self, user_name: str, giphy_id: str, tags: str = None, content: bytes = None, source_url: str = None):
        """Represents the uploaded gif
        :param user_name: the user which uploaded the gif
        :param tags: comma delimited string of descriptive tags
        :param content:
        :param source_url:
        """
        super().__init__(
            giphy_id=giphy_id,
            user_name=user_name,
            upload_date=datetime.utcnow().astimezone(),
            tags=tags
        )

        if content and source_url:
            raise ValueError(GIF_CONTENT_AND_SOURCE_URL_MESSAGE)

        if content:
            self.content = content
        if source_url:
            self.source_url = source_url


class GifSchema(Schema):
    _id = fields.String(data_key='_id')
    giphy_id = fields.String(data_key='giphyId')
    user_name = fields.String(data_key='userName')
    upload_date = custom_fields.DateTimeFormatted(data_key='uploadDate')
    source_url = fields.Url(data_key='sourceUrl')
    tags = fields.String()

    @post_dump
    def remove_empties(self, data, **kwargs):
        if type(data) is dict:
            return {k: v for k, v in data.items() if v is not None}


class GifData:
    COLLECTION_NAME = 'gifs'

    def __init__(self, db_: Connection):
        self._db = db_

    def insert(self, gif: NewGif) -> Gif:
        """Saves the details for a new Giphy gif
        Note: content and source_url are mutually exclusive. ValueError is raised if both are included.

        :param gif: Details for new gif
        :return: Gif details
        :raises InsertFailed if unable to save the data
        :raises ValueError if the request includes both content and source_url
        """

        try:
            # data = GifDataSchema().dump(gif)
            response_gif = self._db.save_and_get(self.COLLECTION_NAME, gif.to_data_doc(), GifSchema, Gif)
            # response_gif = self._db.get_by_id(self.COLLECTION_NAME, i, GifSchema, Gif)
        except InsertFailed as ex:
            raise GifDataError('Failed to insert gif', ex)

        return response_gif

    def get(self, user_name, giphy_id) -> Gif:
        """Retrieves the details for a gif

        :returns Gif: The Gif details.
        """

        query = {'userName': user_name, 'id': giphy_id}
        gif = self._db.get(self.COLLECTION_NAME, query, GifSchema, Gif)
        return gif

    def get_all(self, user_name, skip: int = 0, limit: int = 0) -> list:
        return self._db.get_all(self.COLLECTION_NAME, {'userName': user_name}, schema=GifSchema, output=Gif,
                                skip=skip, limit=limit)
        # schema = GifSchema()
        # yield [schema.loads(json.dumps(i, default=str)) for i in cursor]
