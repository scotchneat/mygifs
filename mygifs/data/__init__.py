

class DBError(Exception):
    """Base exception for the db module."""
    pass


class ConnectionFailure(DBError):
    """Raised when the DB Client is unable to connect to the database server."""
    pass


class InsertFailed(DBError):
    """Raised when an insert action fails."""


class InvalidKey(InsertFailed):
    pass


class DataSerializationError(DBError):
    pass


class DataDeserializationError(DBError):
    pass
