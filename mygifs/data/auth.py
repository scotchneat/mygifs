from datetime import datetime, timedelta

from marshmallow import fields
from marshmallow.schema import Schema

from mygifs.data import custom_fields
from mygifs.data.db import Connection


class Token:
    def __init__(self, token, user, expires: datetime):
        self.token = token
        self.user = user
        self.expires = expires

    def __repr__(self):
        return str(self.token)

    def to_data_doc(self):
        doc = {}
        if self.token:
            doc['_id'] = self.token
        if self.user:
            doc['user'] = self.user
        if self.expires:
            doc['expires'] = self.expires

        return doc


class NewToken(Token):
    def __init__(self, user):
        expires = datetime.utcnow() + timedelta(hours=12)
        super().__init__(token=None, user=user, expires=expires)


class TokenSchema(Schema):
    token = fields.String(data_key='_id')
    user = fields.String()
    expires = custom_fields.DateTimeFormatted()


class TokenData:
    def __init__(self, db: Connection):
        self.db = db
        self.col_name = 'tokens'

    def create(self, user):
        new_token = NewToken(user=user)
        inserted_id = self.db.save(self.col_name, new_token.to_data_doc())
        if inserted_id:
            return inserted_id

    def get(self, token: str):
        return self.db.get_by_id(self.col_name, _id=token, schema=TokenSchema, output=Token)
