"""
Usage:
server.py GIPHY_KEY [--ip=IP] [--port=PORT] [--db-url=DB_URL]
server.py -h | --help | --version

Options:
    --ip=IP  IP Address used to bind the listener to [default: localhost]
    --port=PORT  TCP Port to listen on [default: 8080]
    --db-url=DB_URL  MongoDB URL [default: mongodb://localhost/mygifs]

Info:
    GIPHY_KEY is required and should be a valid API key for Giphy.com
"""
from docopt import docopt

from mygifs import MY_GIFS

if __name__ == "__main__":
    opts = docopt(__doc__)
    ip = opts['--ip']
    port = opts['--port']
    db_url = opts['--db-url']
    giphy_key = opts['GIPHY_KEY']

    MY_GIFS.config.update(
        DB_URL=db_url,
        GIPHY_KEY=giphy_key
    )

    MY_GIFS.run(ip, port)
