import logging

from flask import Blueprint, jsonify, request
from marshmallow import Schema, fields, validate

from mygifs.api import get_db, accepts_json, contains_json, ContentTypeError, validate_schema, requires_auth
from mygifs.constants import PASSWORD_REGEX, PASSWORD_REGEX_ERROR
from mygifs.data.users import UserData, NewUser

LOG = logging.getLogger(__name__)
USERS_API = Blueprint('users', 'users')

PW_VALIDATOR = validate.Regexp(regex=PASSWORD_REGEX, error=PASSWORD_REGEX_ERROR)


class UserRequest(Schema):
    name = fields.String(data_key='userName', required=True,
                         error_messages={'required': 'User name is required.'})
    full_name = fields.String(data_key='fullName', required=True,
                              error_messages={'required': 'Full name is required.'})
    email_address = fields.Email(data_key='emailAddress', required=True,
                                 error_messages={'required': 'Email address is required.'})


class RegisterUserRequest(UserRequest):
    password = fields.String(required=True, validate=PW_VALIDATOR,
                             error_messages={'required': 'Password is required.'})


class RegisterUserResponse(Schema):
    _id = fields.String(data_key='name')
    full_name = fields.String(data_key='fullName')
    email_address = fields.Email(data_key='emailAddress')
    date_registered = fields.DateTime(data_key='dateRegistered')
    validated = fields.Bool()


@USERS_API.route('/users', methods=['POST'])
@validate_schema(RegisterUserRequest)
def register_user(data):
    if not contains_json() or not accepts_json():
        raise ContentTypeError('Invalid content type.')

    db = get_db()
    new_user = NewUser(**data)

    result = UserData(db).register(new_user)
    if result:
        resp = RegisterUserResponse().dump(new_user)
        return jsonify(resp)


@USERS_API.route('/users/<string:name>', methods=['GET', 'DELETE', 'PATCH'])
@requires_auth
def manage_user(name, *, token_user):
    if request.method == 'GET':
        pass
    if request.method == 'DELETE':
        pass
    if request.method == 'PATCH':
        pass
