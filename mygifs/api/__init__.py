from datetime import datetime
from functools import wraps
from typing import Type

from flask import request, g, current_app
from marshmallow import Schema
from werkzeug.exceptions import UnsupportedMediaType, Unauthorized

from mygifs.data import db
from mygifs.data.auth import TokenData
from mygifs.data.users import User


class APIError(Exception):
    pass


class RequestValidationError(APIError):
    def __init__(self, msg, details):
        self.msg = msg
        self.details = details


class ContentTypeError(APIError):
    pass


class DBInstanceError(APIError):
    pass


class SchemaValidationError(APIError):
    def __init__(self, msg, errors):
        self.msg = msg
        self.errors = errors


class MarshallingError(APIError):
    pass


def requires_auth(f):
    @wraps(f)
    def wrapper(*args, **kw):
        token = request.headers.get('auth-token')
        if not token:
            raise Unauthorized
        token_user = validate_token(token)
        return f(token_user=token_user, *args, **kw)

    return wrapper


def validate_schema(s: Type[Schema]):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            if not request.is_json:
                raise ValueError('Expected JSON content.')
            data = request.json
            req_data = s().load(data)
            return f(data=req_data, *args, **kw)

        return wrapper

    return decorator


def requires_params(params: list):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            missing = []
            for p in params:
                if p not in request.args.keys():
                    missing.append(p)
            if missing:
                raise RequestValidationError('Missing parameters', missing)
            return f(*args, **kw)

        return wrapper

    return decorator


def requires_fields(fields: list):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            missing = []
            for field in fields:
                if field not in request.form:
                    missing.append(field)
            if missing:
                raise RequestValidationError('Missing fields', missing)
            return f(*args, **kw)

        return wrapper

    return decorator


def accepts_form(f):
    @wraps(f)
    def wrapper(*args, **kw):
        if 'application/x-www-form-urlencoded' not in request.content_type:
            raise UnsupportedMediaType('expects application/x-www-form-urlencoded')
        return f(*args, **kw)

    return wrapper


def accepts_json():
    return 'application/json' in request.accept_mimetypes


def contains_json():
    return request.content_type == 'application/json'


def accepts_html():
    return 'text/html' in request.accept_mimetypes


def get_db():
    if 'db' not in g:
        db_url = current_app.config['DB_URL']
        g.db = db.Connection(db_url)

    return g.db


def add_authenticated_token(token, user: User):
    if 'tokens' not in g:
        tokens = {token: user}
        g.tokens = tokens
    else:
        g.tokens[token] = user
    return


def validate_token(token):
    data = TokenData(get_db()).get(token)
    expires = data.expires.replace(tzinfo=None)
    if expires < datetime.now():
        raise Unauthorized('invalid token')
    return data.user


def get_giphy_key():
    if 'giphy_key' not in g:
        g.giphy_key = current_app.config['GIPHY_KEY']

    return g.giphy_key
