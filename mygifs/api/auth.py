from flask import Blueprint, jsonify, request

from mygifs.api import accepts_form, requires_fields, get_db
from mygifs.data.auth import TokenData
from mygifs.data.users import UserData

AUTH_API = Blueprint('auth', 'auth')


@AUTH_API.route('/auth', methods=['POST'])
@accepts_form
@requires_fields(['userName', 'password'])
def generate_auth_token():
    user_name = request.form.get('userName')
    password = request.form.get('password')
    db = get_db()
    user = UserData(db).get(user_name)
    if user.verify_password(password):
        token = TokenData(get_db()).create(user=user_name)
        return jsonify({'token': str(token)})
