from flask import Blueprint, jsonify, request
from marshmallow import Schema, fields

from mygifs.api import accepts_form, get_db, requires_auth
from mygifs.data.gifs import GifData, NewGif

GIFS_API = Blueprint('gifs', 'gifs')


class GifResponse(Schema):
    user_name = fields.String(data_key='userName')
    giphy_id = fields.String(data_key='giphyId')
    tags = fields.String()
    upload_date = fields.DateTime(data_key='uploadDate')
    content = fields.String()
    source_url = fields.String(data_key='sourceUrl')


@GIFS_API.route('/gifs', methods=['POST'])
@accepts_form
@requires_auth
def upload_gif(token_user):
    tags = request.form.get('tags')
    content = request.form.get('content')
    source_url = request.form.get('sourceUrl')
    new_gif = NewGif(user_name=token_user, tags=tags, content=content, source_url=source_url)

    db = get_db()
    results = GifData(db).insert(gif=new_gif)
    resp = GifResponse().dump(results)
    return jsonify(resp)


@GIFS_API.route('/gifs', methods=['GET'])
@requires_auth
def get_gif_list(token_user):
    db = get_db()
    results = GifData(db).get_all(user_name=token_user)
    resp = GifResponse().dump(results, many=True)
    return jsonify(resp)


@GIFS_API.route('/gifs/<gif_id>', methods=['GET', 'DELETE', 'PATCH'])
def gif(gif_id):
    user = request.args.get('user')

    db = get_db()
    results = GifData(db).get_all(user_name=user)
    resp = GifResponse().dump(results, many=True)
    return jsonify(resp)
