from flask import Flask, jsonify
from marshmallow import ValidationError

from mygifs.api import RequestValidationError
from mygifs.api.auth import AUTH_API
from mygifs.api.users import USERS_API
from mygifs.api.gifs import GIFS_API
from mygifs.data.users import UserExistsException

MY_GIFS = Flask(__name__)

MY_GIFS.register_blueprint(USERS_API)
MY_GIFS.register_blueprint(GIFS_API)
MY_GIFS.register_blueprint(AUTH_API)


@MY_GIFS.errorhandler(RequestValidationError)
def handle_request_validation_error(e: RequestValidationError):
    return jsonify({e.msg: e.details}), 400


@MY_GIFS.errorhandler(ValidationError)
def handle_schema_validation_error(e: ValidationError):
    return jsonify({'schemaValidationError': e.messages}), 400


@MY_GIFS.errorhandler(ValueError)
def handle_schema_validation_error(e: ValueError):
    return jsonify({'valueError': str(e)}), 400


@MY_GIFS.errorhandler(UserExistsException)
def handle_schema_validation_error(e):
    return jsonify({'error': 'user name or email address is already registered'}), 400


@MY_GIFS.errorhandler(NotImplementedError)
def handle_schema_validation_error(e):
    return jsonify({'error': 'not implemented'}), 501