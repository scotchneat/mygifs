import datetime

from mygifs.constants import DT_FORMAT


def json_serializer_default(o):
    if type(o) is datetime.datetime:
        return o.astimezone().strftime(DT_FORMAT)
    return str(o)
