
PASSWORD_REGEX = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,25}$'

PASSWORD_REGEX_ERROR = 'Password must be 8 - 25 characters and must contain capital and lowercase letters, ' \
                       'numbers, and special characters - [#$^+=!*()@%&]'

DT_FORMAT = '%Y-%m-%dT%H:%M:%S.%f%z'

GIF_CONTENT_AND_SOURCE_URL_MESSAGE = 'should not include both [content] and [source_url]'
