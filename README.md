# MyGIFs


This application allows users to upload and manage gifs on Giphy.com.
Users are able to upload, tag (categorize), and search other user's gifs.

# Current State
> Under development

This project is in the begining stages of development and is subjet to cahnges as features are being impmented.

# Features

* **API** - A [Flask](http://flask.pocoo.org/) backend API. Used to handle the core logic of the application.
* **Database** - [MongoDB](https://www.mongodb.com/) is used to store document data


# Roadmap
These features were planned for the initial build of the project. They will most likely be "fast follows".

**Web UI**

A lightweight web interface built using [React](https://reactjs.org/) and [Bootstrap](https://getbootstrap.com/). Implementation is planned very soon.

**Giphy Upload API Integration**

At the start of this project, the GiphY Upload API seems to be very unstable. In fact, it consistently returned 500 errors (their server side). I'll continue to investigate and test this implementaiotn.

**JWT Auth**

For a more seamless experience between the UI and the API. The use of JSON Web Token authentication will be implemented in the API and supported in the UI.

**Asynchrounous uploads**  
 
 The application will handle the requests to the Giphy API asynchronously. 
 This can be accomplished now by adding Celery workers (requires a message broker).
 
 Alternatively, the built in (python 3.4+) async/await commands can be used to generate coroutines to handle the async logic.
 This option will require an AsyncIO supported web server/framework like AIOHttp or Quark.  

 # Installation

 Se the [Installation Guide](INSTALLATION.md) for instructions on setting up and running the application in the current state.


# API
Checkout the [API Docs](API.md) for full API details.