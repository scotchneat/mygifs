# Installation

## System Requirements

Before getting the application installed, make sure you have the following installed and configured on your system.

* **Linux** - While this can all be set up to run on Windows, these instructions are targed for linux-based users.
* **Git**
* **Python 3.7** - I suggest using [pyenv](https://github.com/pyenv/pyenv) to manage your python installation versions and virtual environments.
* **Docker**
* **Giphy API Key** - This is not a hard requirement yet since the integration is not started.

## Steps

1. Clone this repository

    From your preferred installation directory, Run the clone command:

    ```bash
    git clone git@gitlab.com:scotchneat/mygifs.git
    ```

    Then change directories to the repo directory

    ```bash
    cd mygifs
    ```

2. Install python dependencies  

    > Before installing the dependencies, this is where you should initialize the virtual directory

    A [Makefile](Makefile) has been started to expose convenience commands. for more detailsa bout the commands, 
    ```bash
    make dev
    ```

3. Start docker images  

    Once Docker has been installed on your system, you can run the following to bring the dependency images online:

    ```bash
    make docker
    ```

    > Note: this will launch docker-compose up in detatched state.

4. Run the application  

    The make target for this step expects the `$GIPHY_API_KEY` environment variable.
    Since Giphy integration is not currently in place, you can export anything to this value.

    ```bash
    export GIPHY_API_KEY=YourKeyHere
    ```

    Then you can run the application

    ```bash
    make serve
    ```

5. Use the API

    You can now make HTTP requests to the API

    ```bash
    curl -X POST \
        http://localhost:8080/users \
        -H 'Content-Type: application/json' \
        -d '{"userName": "youruser", "fullName": "Your Name", "password": "S3cur3P@ss", "emailAddress": "your@email.com"}'
    ```

# API
Checkout the [API Docs](API.md) for full API details.