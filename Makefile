

dev: ## Installs python dependency packages
	pip install -e .[test]

serve: ## Run the Flask dev server
	python mygifs/server.py $(GIPHY_API_KEY) --ip=localhost --port=8080

docker: ## Startup docker images
	docker-compose up -d